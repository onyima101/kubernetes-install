play-with-docker.com
https://training.play-with-docker.com/
git.docker.com


WHAT IS A CONTAINER:
- Executable unit of software
- Encapsulates everything necessary to run 
- Can be run on any platform
- OS-Level virtualization
- Isolates processes
- Controls resources allocated to those processes
- Small, fast and portable
- Doesn't include a guest OS in every instance rather, it leverages host OS.

BENEFITS OF CONTAINERS:
- Lightweight 
- Don't include a guest os
- Spin up quickly and scale horizontally 
- Portable and platform independent
- Support modern development and architecture
- Improve utilization
- scale each component individually

USE CASES FOR CONTAINERS:
- Microservices: Loosely coupled and independently deployable services
- DEVOPS: Build, deploy and run software
- Hybrid, Multi-Cloud: Run consistently across environments
- Application modernization and migration.

WHAT IS DOCKER:
- Software platform for building and running containers
- Containerization existed before docker
- Lunch of docker in 2013 popularized containerization
- Provides a straightforward way to build and run containers.

DEVELOPMENT OF A CONTAINER:
- A docker file serves as the blueprint for the image. It outlines steps to build the image.
- An image is an immutable file that contains everything necessary to run an application. It is read-only.
- A container is a running image. I.e. Write layer is put on top of the read-only image.
- Dockerfile ===> image ===> Container

IMAGE LAYERS:
- Images are built using instructions in a Dockerfiles
- Each docker instruction creates a new read-only layer
- A writeable layer is added when an image is run as a container
- Layers can be shared btw images, which saves disk space and network bandwidth.

DOCKERFILE INSTRUCTIONS:
- FROM: Define the base image 
- RUN: Execute arbitrary commands
- ENV: Set environment variables 
- ADD/COPY: Add files from remote location VS Copy files and directories from local location
- CMD: Define default command for container execution.

CONTAINER REGISTRY:
- Storage and distribution of named/tagged container images
- public or private registry
- Hosted or self-hosted registry

IMAGE NAMING/TAGGING
- Hostname/repository:tag
Eg docker.io/ubuntu:18.04  or us.icr.io/ubuntu:18.04

RUNNING CONTAINERS:
BUILD: 
- Build from Dockerfiles 
Docker build -t my-app:v1 .

TAG:
- docker tag my-app:v1 second-app:v1

RUN:
- docker run my-app:v1

PUSH:
- docker push my-app:v1








**************************************************************************
docker search redis
docker run -d redis
docker ps 
####  -p <host-port>:<container-port>
#### By default, the port on the host is mapped to 0.0.0.0, which means all IP addresses. You can specify a particular IP address when you define the port mapping, for example, -p 127.0.0.1:6379:6379
## for specific port mapping
docker run -d --name redisHostPort -p 6379:6379 redis:latest
## for random generated port
docker run -d --name redisDynamic -p 6379 redis:latest
docker port redisDynamic 6379

##Binding directories (also known as volumes) is done using the option -v <host-dir>:<container-dir>
## the official Redis image stores logs and data into a /data directory.Any data which needs to be saved on the Docker Host, and not inside containers, should be stored in /opt/docker/data/redis.
docker run -d --name redisMapped -v /opt/docker/data/redis:/data redis

## To run as shell
docker run ubuntu ps
docker run -it ubuntu bash

##BUILDING CONTAINERS
vi dockerfile
*
FROM <image-name>:<tag>
COPY <src> <dest>
EXPOSE <80 443> <7000-8000>
CMD ["cmd", "-a", "arga value", "-b", "argb-value"] 

eg

FROM nginx:1.11-alpine
COPY index.html /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

##docker build -t <name> .
##The -t parameter allows you to specify a friendly name for the image and a tag
##NOTE:: create directory. cp dockerfile and index.html to directory.

docker build -t nd-nginx-image:v1 .
docker run -d -p 80:80 --name pvlva101 nd-nginx-image:v1
curl http://docker

##Verify image created correctly
docker images --filter reference=ubuntu-image

##copy container config file
docker cp tmp-nginx-container:/etc/nginx/nginx.conf /root/docker/nginx.conf
**************************************************************************


docker pull httpd
docker run -d -P --name web nginx
docker container create [option] image [command] [argument] 
docker run -d -t -p 80:80 --name lvlva101 centos
## Binding directories (also known as volumes) is done using the option -v <host-dir>:<container-dir>
docker run -dit --name lvlva101 -P -v /home/website:/usr/local/apache2/htdocs httpd
docker port lvlva101
docker inspect lvlva101 | grep -i ipaddress
docker exec -it lvlva101 bash
docker stop/start lvlva101
docker container top lvlva101
docker container logs lvlva101
docker container stats lvlva101 
docker container inspect lvlva101
###for all containers###
docker container stats -a 

##View container IP Address
docker container  inspect --format "{{.NetworkSettings.IPAddress}}" dvlva101

##ports
docker container port lvlva101

CONNECT TO CONTAINERS:
##for new containers
docker run --help

##for running containers
docker exec --help

##for existing containers that are not running
docker start --help

##networks
docker network ls
1, Bridge Network: Default docker virtual network, which is NAT'ed behind the host ip.
Host OR Default network that bridges thru the NAT firewall to the physical network which your host is connected to.

2, Host network: It gains performance by skipping the virtual networks but sacrifices security of the container model.

3, none network: Removes eth0 and only leaves you with localhost interface in container.

##network commands
docker network ls/inspect/create/connect/disconnect --help

WHAT IS DOCKER IMAGE:
Its the app binaries and dependencies and the metadata of how to run it.

##DOCKER COMMIT
docker commit -m "added nodejs to container" -a "nonyima" pvlva101 onyima101/nginx-nodejs


TAGS
docker image inspect <imagename>
docker image tag source-image target-image 
docker login
docker image push --help (NOTE: login 1st and logout later)

CLEAN UP
docker system df (check space useage)
docker system prune (clean everything)
docker image prune (clean up images)
docker image prune -a (clean all images)

##VOLUMES
## -v gives the volume a name:full-path its mounted on the container with reference to the dockerfile.
 dc run -d --name mysql2 -e MYSQL_ALLOW_EMPTY_PASSWORD=True -v mysql-vol:/var/lib/mysql mysql

##POSTGRES
POSTGRES_PASSWORD=mypasswd OR POSTGRES_HOST_AUTH_METHOD=trust
##To login to database
psql -h localhost -p 5432 -U postgres -d postgres

docker run -d --name postgres-db1 --network postgres -v postgres-vol:/var/lib/postgresql/data -e POSTGRES_PASSWORD=mysecretpassword postgres:9.6.1


##JEKYLL
d run -it --name jekyll-server -p 80:4000 -v $(pwd):/site onyima101/jekyll:1.0


##SWARM
docker info
docker swarm init 
docker node ls
docker service 

##CREATE MORE NODES FOR DOCKER SWARM
docker-machine create node1
#To ssh into node1, do
docker-machine ssh node1
##To start
docker-machine start node1

##To add new node to swarm
docker swarm join-token --rotate worker
docker swarm join-token manager

##To leave the swarm from particular node
docker swarm leave 
##To remove node from node list
docker node rm <node-name>

## To update node role to manager
docker node update --role manager node2
docker node promote node2

docker service create --replicas 3 alpine ping 8.8.8.8
d service ls
d node ps
d node ps node1
d service ps serene_antonelli



##STACKS
##To deploy a stack 
## docker stack deploy -c <docker-compose.yml> <stack-name>
docker stack deploy -c example-voting-app-stack.yml voteapp

NOTE: Stack ==> services ==> tasks ==> containers

##To view the services
docker stack services voteapp

##To view the tasks
docker stack ps voteapp

##If .yaml file is modified, then rerun stack deploy command again.



##SECRETS on swarm only
##con = secret is locally on host
docker secret create psql_user psql_user.txt

## con = secret is on history command
echo "psqlPASSWORD" | docker secret create psql_pass -

##create service using the secrets
d service create --name psql --secret psql_user --secret psql_pass -e POSTGRES_PASSWORD_FILE=/run/secrets/psql_pass -e POSTGRES_USER_FILE=/run/secrets/psql_user postgres

NOTE: ssh into the containers to verify the secrets are on the path.



##SERVICE UPDATES
docker service create -p 8088:80 --name web nginx:1.13.7
docker service scale web=5 
docker service update --image nginx:1.13.6 web
docker service rollback web
#To update the port
docker service update --publish-rm 8088 --publish-add 8080:80 web
#To balance containers on nodes, update by 2's, delay 10sec
docker service update --force  --update-parallelism 2 --update-delay 10s web


##HEALTH CHECKS
HEALTHCHECK [OPTIONS] CMD command
The options that can appear before CMD are:

--interval=DURATION (default: 30s)
--timeout=DURATION (default: 30s)
--start-period=DURATION (default: 0s)
--retries=N (default: 3)

d run --name p1 -e POSTGRES_PASSWORD=password -d postgres
d run --name p2 -e POSTGRES_PASSWORD=password -d  --health-cmd="pg_isready -U postgres || exit 1"  postgres
#The "docker ps" command shows the health-check
docker service create  --name p1 -e POSTGRES_PASSWORD=password postgres
docker service create --name p2 -e POSTGRES_PASSWORD=password --health-cmd="pg_isready -U postgres || exit 1"  postgres



##PRIVATE DOCKER REGISTRY
##create registry container
d run -d -p 5000:5000 --name registry registry
##tag image to localhost
d tag onyima101/mynode:1.0 127.0.0.1:5000/mynode1
##push to private registry container
d push 127.0.0.1:5000/mynode1
##pull image, kill container, create container with bind mount
d run -d -p 5000:5000 --name registry -v $(pwd)/registry-data:/var/lib/registry registry
##push to new registry with bind mount and view bind mount directory
d push 127.0.0.1:5000/mynode1


##REGISTRY WITH SSL CERTS
vi /etc/docker/daemon.json
{ "insecure-registries":["host:port"] }


did not work for me until I created the file

vi /etc/default/docker
DOCKER_OPTS="--config-file=/etc/docker/daemon.json"

#restart service 
systemctl restart docker 

##generate ssl certs
mkdir -p certs 
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt

##To get the docker daemon to trust the certificate, copy the domain.crt file.
mkdir /etc/docker/certs.d
mkdir /etc/docker/certs.d/127.0.0.1:5000 
cp $(pwd)/certs/domain.crt /etc/docker/certs.d/127.0.0.1:5000/ca.crt

##Make sure to restart the docker daemon.
pkill dockerd
dockerd > /dev/null 2>&1 &

##run registry securely
mkdir registry-data
docker run -d -p 5000:5000 --name registry \
  --restart unless-stopped \
  -v $(pwd)/registry-data:/var/lib/registry -v $(pwd)/certs:/certs \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  registry

##Create username and password
mkdir auth
docker run --entrypoint htpasswd registry:2.7.0 -Bbn moby gordon > auth/htpasswd
cat auth/htpasswd

##Create authenticated secure registry
docker rm -f registry
docker run -d -p 5000:5000 --name registry \
  --restart unless-stopped \
  -v $(pwd)/registry-data:/var/lib/registry \
  -v $(pwd)/certs:/certs \
  -v $(pwd)/auth:/auth \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -e REGISTRY_AUTH=htpasswd \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
  -e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" \
  registry
##authenticate before image pull
docker login 127.0.0.1:5000
docker pull 127.0.0.1:5000/hello-world

##to view registry content on web
http://ipaddress:port/v2/_catalog

##file location on local machine
/var/lib/docker/volumes/*****/_data/docker/registry/v2/repositories/
